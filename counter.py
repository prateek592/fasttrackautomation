import threading
import logging
import EtollIDFC
from datetime import datetime
import csv

class Counter(object):
   def __init__(self, start = 0):
       self.lock = threading.Lock()
       self.value = start
   def lockCSV(self,vehicleRegNo , msg , threadNo , balance , amountDeducted ,path):
       # print('Waiting for CSV')
       self.lock.acquire()
       try:
           # print('Acquired CSV')
           self.editingCSV(vehicleRegNo , msg , threadNo , balance , amountDeducted ,path)
           self.value = self.value + 1
       finally:
           # print('Released CSV')
           self.lock.release()
   def editingCSV(self ,vechileRegNo, msg, threadNo, balance, deducted ,path):
       line_count = 0
       csv_loc = path + "\\fasttrackvehcsv.csv"
       with open(csv_loc, 'r') as f:
           reader = csv.reader(f, delimiter=',')
           lines = []
           for line in reader:
               now = datetime.now()
               if line[0] == str(vechileRegNo):
                   line[1] = str(msg)
                   line[2] = int(threadNo)
                   line[3] = now.strftime("%d/%m/%Y %H:%M:%S")
                   line[4] = float(balance)
                   line[5] = float(deducted)
                   line_count += 1
               lines.append(line)
           f.close()
       csv_loc = path + "\\fasttrackvehcsv.csv"
       with open(csv_loc, 'w', newline='') as f:
           writer = csv.writer(f, delimiter=',')
           writer.writerows(lines)
           f.close()