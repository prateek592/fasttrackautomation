from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from Recharge import recharge
from selenium.common.exceptions import WebDriverException
import urllib.parse
import requests
class EtollIDFC(recharge):
    def __init__(self,driver):
        self.spr = super(EtollIDFC, self)
        try:
            self.driver = driver
        except Exception as e:
            print(e)
            #sys.exit()
        self.spr.__init__("1", self.driver)

    def check_exists_by_id_etoll(self, id):
        try:
            self.spr.checkPresenceOfElement("id",id)
        except NoSuchElementException:
            return False
        return True

    def check_exists_by_clss_etoll(self, clss):
        try:
            self.driver.find_element_by_class_name(clss)
        except NoSuchElementException:
            return False
        return True

    def waitForClass(self , clss):
        counter = 1
        while (self.check_exists_by_clss_etoll(clss)):
            self.spr.timeSleep(2)
            counter = counter + 1
            # print("Counter -- >",counter)
            if counter >= 7:
                return False
            continue
        self.spr.timeSleep(3)
        return True

    def EtollIDFC(self,  num, threadNo):
        try:
            print("================= ETOLL IDFC STARTING ====================")
            self.spr.timeSleep(5)
            submit = self.check_exists_by_id_etoll('pay')
            if submit == True:
                # vehicleRegNo = sheet.cell_value(6, 0)
                vehicleRegNo = num
                self.driver.execute_script("document.getElementById('custId').setAttribute('style', 'display:block')")
                select = Select(self.driver.find_element_by_id('custId'))
                select.select_by_visible_text(vehicleRegNo)
                overlayResponse=self.waitForClass('blockOverlay')
                # print("overlay response-->",overlayResponse)
                if overlayResponse == False:
                    # print("Under IF")
                    self.driver.execute_script("location.reload()")
                    msg = ['0', "This Vehicle Taking More Time Than Usual", '0']
                    print(msg)
                    return msg
                value = self.driver.find_element_by_id("closingBalanceFrom")
                vehicleAmount = value.get_attribute("value")
                print("vehicle amount",vehicleAmount)
                # print(value.get_attribute("value"))
                self.spr.timeSleep(3)
                vehicleAmount = float(vehicleAmount)
                # print("vehicle amount->",vehicleAmount)
                # vehicleAmount = float(vehicleAmount1)
                # print(vehicleAmount)
                if vehicleAmount > 1:
                    self.driver.execute_script("document.getElementById('custId1').setAttribute('style', 'display:block')")
                    select = Select(self.driver.find_element_by_id('custId1'))
                    select.select_by_visible_text('POOL ACCOUNT')
                    self.waitForClass('blockOverlay')
                    amountDeducted = vehicleAmount - 1.0
                    amountDeducted = round(amountDeducted, 2)
                    # print("amount deducted-->",amountDeducted)
                    amountDeducted = str(amountDeducted)
                    # print("amount deducted-->", amountDeducted)
                    amount = self.driver.find_element_by_id("amount")
                    self.driver.execute_script('''var elem = arguments[0];var value = arguments[1];elem.value = value;''', amount, amountDeducted)
                    self.spr.timeSleep(3)
                    self.driver.find_element_by_id("pay").click()
                    self.waitForClass('blockOverlay')
                    successResponse = self.driver.find_element_by_xpath('//*[@id="mainalertspan"]/p/strong').get_attribute('innerHTML')
                    # print(successResponse)
                    alertResponse = self.spr.checkPresenceOfElement('class', 'alert').get_attribute('innerText')
                    # print(alertResponse)
                    if successResponse == "Success ! ":
                        # alertResponse = self.driver.find_element_by_xpath('//*[@id="mainalertspan"]/p/text()').get_attribute('innerHTML')
                        msg = alertResponse.replace("\n", "")
                        msg = msg[1:]
                        # print("msg-->",msg)
                        self.spr.timeSleep(5)
                        msg = [amountDeducted , msg , vehicleAmount ]
                        return msg
                    else:
                        # print(successResponse)
                        # alertResponse = self.driver.find_element_by_xpath('//*[@id="mainalertspan"]/p/text()').get_attribute('innerHTML')
                        msg = alertResponse.replace("\n", "")
                        msg = msg[1:]
                        # print("msg-->",msg)
                        self.spr.timeSleep(5)
                        msg = [amountDeducted , msg , vehicleAmount]
                        return msg
                else:
                    print("Amount is less than 1 of vehicle ID-->{vehno}".format(vehno=vehicleRegNo))
                    msg1 = "Amount is less than 1 of vehicle ID-->{vehno}".format(vehno=vehicleRegNo)
                    msg = ['0', msg1 , vehicleAmount]
                    return msg


        except Exception as e:
            print(e)
            error = "Element Not Found Thread {}".format(threadNo)
            # print("error--->",error)
            print("Error is -->{}".format(error))
            msg = ['0',e,'0']
            return msg









