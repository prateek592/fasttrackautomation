import os
from bs4 import BeautifulSoup
import time
import requests
import urllib.parse
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging
import threading
from datetime import datetime

class recharge(object):
    def __init__(self, recharge_id, driver):
        recharges = {
            "1": "EtollIDFC",
        }

        self.recharge_id = recharge_id
        self.recharge_name = recharges[recharge_id]
        self.driver = driver

    def add_to_log(self, level, msg):
        try:
            logger = logging.getLogger(self.recharge_name)
            logger.setLevel(logging.INFO)

            #create all level file handler
            log_path1 = os.getcwd()+"\\Log\\all_log.log"
            handler1 = logging.FileHandler(log_path1)
            handler1.setLevel(logging.INFO)

            #create ERROR level file handler
            log_path2 = os.getcwd()+"\\Log\\error_log.log"
            handler2 = logging.FileHandler(log_path2)
            handler2.setLevel(logging.ERROR)

            #create a logging format
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            handler1.setFormatter(formatter)
            handler2.setFormatter(formatter)

            #add the handlers to the logger
            logger.addHandler(handler1)
            logger.addHandler(handler2)

            if level == 'info':
                logger.info(msg)
            elif level == 'error':
                logger.error(msg)
        except Exception as e:
            print (e)
            self.driver.quit()

    def checkPresenceOfElement(self, method, args):
        wt = 200
        try:
            if method=='name':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.NAME, args)))
                return element
            elif method=='xPath':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.XPATH, args)))
                return element
            elif method=='id':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.ID, args)))
                return element
            elif method=='class':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.CLASS_NAME, args)))
                return element
            elif method=='link_text':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.LINK_TEXT, args)))
                return element
            elif method=='css':
                element = WebDriverWait(self.driver, wt).until(EC.presence_of_element_located((By.CSS_SELECTOR, args)))
                return element
        except Exception as e:
            print(e)








    def returnTableData(self,tableid ):
         try:
             table_id = self.driver.find_element_by_id(tableid)
             rows = table_id.find_elements_by_tag_name("tr")  # get all of the rows in the table
             for row in rows:
                 col = row.find_elements_by_tag_name ("td")[1]  # note: index start from 0, 1 is col 2
                 return col.text
         except Exception as e:
             print(e)

    def timeSleep(self, time1):
        try:
            time.sleep(time1)
        except Exception as e:
            print(e)


    def notify(self, level, msg):
        try:
            with open('notificationFlag.txt','r+') as f:
                preMsg = f.read()
                winEr = "WinError"
                sbi_er = "'NoneType' object has no attribute 'click'"
                if (msg == preMsg) or (winEr in msg) or ((sbi_er in msg) and ("Invalid" not in msg)):
                    f.close()
                    return
                f.close()

            with open('notificationFlag.txt','w+') as f:
                f.write(msg)
                f.close()

        except Exception as e:
            print(e)
            self.driver.quit()

    def failed(self, msg, userid):
        # print(userid)
        self.notify('error', msg)
        with open('uploadurl.txt', "r+") as f:
            uploadURL = f.read()
        print("Last Automatic Session -- Status - 'Failed' , Time - " + str(datetime.today()))
        url = uploadURL+'FoFuelTransactions/updateHpclCrediantials'
        print(url)

        data = {
            'userid': userid,
            'remarks': msg,
                }
        headers = {'Content-Type': 'application/json'}
        r = self.apiCall(url,data ,headers)
        print(r.text)
        # file = os.getcwd() + "\\Log/error_log.log"
        # f = open('apihitflagtransaction.txt', 'w+')
        # f.write("0")
        # f.close()
        # os.startfile(file)
        self.driver.quit()

    def success(self):
        with open('notificationFlag.txt', 'r+') as f:
            if len(f.read()) > 0:
                self.notify('INFO', "Last Automatic Session -- Status - 'Success'")
            f.close()

        with open('notificationFlag.txt', 'w+') as f:
            f.write("")
            f.close()

        print("Last Automatic Session -- Status - 'Success'")
        self.add_to_log('info', 'Successfully Logout ......')
        self.driver.quit()

    def htmlPageToTable(self, tableid):
        try:
            # tableid = 'ctl00_phM_fsTransactionSummary'
            # print(tableid)
            element = self.checkPresenceOfElement("id" , tableid)
            os.remove("page.html")
            with open('page.html', 'w') as f:
                f.write(element.get_attribute('innerHTML'))
            html = open("page.html").read()
            soup = BeautifulSoup(html, 'html.parser')
            if soup.find("table"):
                table = soup.find("table")
                return table
            else:
                return None
        except Exception as e:
            print(e)

    def apiCall(self, url , data, headers):
        try:
            r = requests.post(url=url, json=data, headers=headers)
            return r
        except Exception as e:
            print(e)





