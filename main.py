#!/usr/bin/env python3
import sys
import os
import urllib.parse
import xlrd
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
import threading
import csv
from threadstop import StoppableThread
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
import time
from Recharge import recharge
import logging
import requests
from datetime import datetime
path = os.getcwd()
# print(path)
vehicleObj = []
vehicleRegNoRemove = []
threadCount = 4
isSaveCSV = False
from counter import Counter

def checkPresenceOfElement(driver ,method, args):
    wt = 200
    try:
        if method == 'name':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.NAME, args)))
            return element
        elif method == 'xPath':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.XPATH, args)))
            return element
        elif method == 'id':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.ID, args)))
            return element
        elif method == 'class':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.CLASS_NAME, args)))
            return element
        elif method == 'link_text':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.LINK_TEXT, args)))
            return element
        elif method == 'css':
            element = WebDriverWait(driver, wt).until(EC.presence_of_element_located((By.CSS_SELECTOR, args)))
            return element
    # except TimeoutException:
    #     self.add_to_log('error','Internet is too slow or element is not found or Session out')
    #     self.failed('Internet is too slow or element is not found or Session out')
    except Exception as e:
        print(e)
        # self.add_to_log('error',e)
        # self.failed(str(e))
def timeSleep(time1):
    try:
        time.sleep(time1)
    except Exception as e:
        print(e)
def messageToSend(error):
    error = urllib.parse.quote(error)
    mobileNo = "9971128066,7340034373"
    # mobileNo = "8824819827,7740949026"
    r = requests.get(
        url="http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=" + mobileNo + "&msg=Hi%20There%0A%20%20%20%20%20%20%20%20%20%20%20%20There%20is%20a%20problem%20with%20Ticketing%20System.%20Possible%20reason%3A%20"+ error +"&userid=2000168101&auth_scheme=plain&password=1Fe7zhsxV&v=1.1&msg_type=text&mask=WALLeT"
    )
    print(r.text)

def check_exists_by_id_fasttrack(driver , id):
    try:
        driver.find_element_by_id(id)
    except NoSuchElementException:
        return False
    return True


def vechReg(threadNo ,login , pwd , totalLogins):
    try:
        ex_path = os.getcwd() + "\\chromedriver.exe"
        # print(ex_path)
        os.environ["webdriver.chrome.driver"] = ex_path
        options = webdriver.ChromeOptions()
        driver = webdriver.Chrome(options = options)
        recharge_path = path
        # print("recharge path-->",recharge_path)
        sys.path.append(recharge_path)
        os.chdir(recharge_path)
        URL = "https://etoll.idfcbank.com/dimtsportal/login"
        driver.get(URL)
        driver.maximize_window()
        checkPresenceOfElement(driver ,'id', 'j_username').send_keys(login)
        checkPresenceOfElement(driver , 'id', 'j_password').send_keys(pwd)
        timeSleep(totalLogins * 7)
        checkPresenceOfElement(driver, "id", "login").click()
        alert = check_exists_by_id_fasttrack(driver , "authDiv")
        # print("alert-->", alert)
        if alert == False:
            print("Starting For Thread No To Automate->{}".format(threadNo))
            checkPresenceOfElement(driver ,"xPath", '//*[@id="wrap"]/div[2]/div/div/div[2]/a[2]/div').click()
            timeSleep(5)
            vehicleRegNo = vehicleAllocation(threadNo)
            # print("vehcile dcdff-->",vehicleRegNo)
            while (vehicleRegNo != None):
                from EtollIDFC import EtollIDFC
                c = EtollIDFC(driver)
                response = c.EtollIDFC(vehicleRegNo, threadNo)
                os.chdir("..")
                # print(response)
                amountDeducted = float(response[0])
                balance = float(response[2])
                msg = str(response[1])
                msg = msg.replace("\n","")
                exception1 = "Message: no such window:"
                exception2 = "Message: chrome not reachable"
                print("msg-->",msg)
                # print("vehicle reg no ---> ", vehicleRegNo)
                c = Counter()
                c.lockCSV(vehicleRegNo , msg , threadNo , balance , amountDeducted ,path)
                if exception1 in msg or exception2 in msg:
                    print("yes")
                    error = "Browser Closed ,Thread Is {}".format(threadNo)
                    messageToSend(error)
                    break
                index = findKeyValueIndex(vehicleRegNo)
                # print(index)
                vehicleObj[index]['Deducted'] = amountDeducted
                vehicleObj[index]['Balance'] = balance
                vehicleObj[index]['msg'] = msg
                vehicleRegNo = vehicleAllocation(threadNo)

            if vehicleRegNo == None:
                # print("New Vehicle Obj--->",vehicleObj)
                checkPresenceOfElement(driver ,"xPath",'//*[@id="wrap"]/div[1]/div[1]/div/div[2]/ul/li[2]/a/font').click()
        else:
            error = checkPresenceOfElement(driver,"xPath",'//*[@id="authDiv"]/p').get_attribute('innerHTML')
            error = error[:30]
            error = error + "  Thread is {}".format(threadNo)
            # print("error--->",error)
            print("Error is -->{}".format(error))
            messageToSend(error)
            driver.quit()
    except Exception as e:
        print(e)
        error = "Possible Reason Logout Thread {}".format(threadNo)
        messageToSend(error)


def findKeyValueIndex(vehicleRegNo):
    lengthVechobj = len(vehicleObj)
    i = 0
    while (lengthVechobj > 0 ):
        if vehicleRegNo == vehicleObj[i]['vechRegno']:
            return i
        lengthVechobj = lengthVechobj - 1
        i = i + 1
def vehicleAllocation(threadNo):
    try:
        for i in range(len(vehicleObj)):
            vehicleRegNo = vehicleObj[i]['vechRegno']
            if vehicleObj[i]['isTaken'] == False:
                vehicleObj[i]['isTaken'] = True
                vehicleObj[i]['threadNo'] = threadNo
                return vehicleRegNo
        return None
    except Exception as e:
        print(e)



if __name__ == "__main__":
    counter = Counter()
    # creating thread
    csv_loc = os.getcwd() + "\\fasttrackvehcsv.csv"
    with open(csv_loc, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            if line[1] == '':
                vehicleDict = {
                    "vechRegno": line[0],
                    "msg": None,
                    "isTaken" : False,
                    "threadNo": None,
                    "Time": None,
                    "Balance": None,
                    "Deducted": None
                }
                vehicleObj.append(vehicleDict)

    # print(len(vehicleObj))
    # print(os.getcwd())
    if len(vehicleObj) <= 0:
        print("There Are No Vehicle No. That Are Left To Automate")
        sys.exit()
    loc = os.getcwd() + "\\LoginCrediantials.xlsx"
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    totalLogins = sheet.nrows
    rows = sheet.nrows + 1
    # print(rows)
    d = {}
    t = []
    for i in range(1,rows):
        login = sheet.cell_value(i-1, 0)
        password = sheet.cell_value(i-1, 1)
        t.append(threading.Thread(target=vechReg, args=(str(i),str(login),str(password), totalLogins)))
        t[len(t)-1].start()








